import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutpageComponent } from './pages/views/aboutpage/aboutpage.component';
import { ContactpageComponent } from './pages/views/contactpage/contactpage.component';
import { FaqpageComponent } from './pages/views/faqpage/faqpage.component';
import { HomepageComponent } from './pages/views/homepage/homepage.component';
import { PagenotfoundComponent } from './pages/views/pagenotfound/pagenotfound.component';
import { VotepageComponent } from './pages/views/votepage/votepage.component';

const routes: Routes = [
	{
		path: "",
		component: HomepageComponent,
	},
	{
		path: "home",
		component: HomepageComponent,
  },
  {
    path: "about",
    component: AboutpageComponent,
  },
  {
    path: "faqs",
    component: FaqpageComponent,
  },
  {
    path: "contact",
    component: ContactpageComponent,
  },
  {
    path: "vote",
    component: VotepageComponent,
  },
	{
		path: "**",
		component: PagenotfoundComponent,
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
