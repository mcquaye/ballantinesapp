import { Component, OnInit } from '@angular/core';
import { SeoService } from "src/app/services/seo/seo.service";

@Component({
	selector: "app-votepage",
	templateUrl: "./votepage.component.html",
	styleUrls: ["./votepage.component.scss"],
})
export class VotepageComponent implements OnInit {
	seoData: any = {
		title: "Vote | Ballantines - Vote For More",
		description: "",
		keywords: "",
	};

	constructor(private seo: SeoService) {
		this.seo.setSeo(this.seoData);
	}

	ngOnInit(): void {}
}
