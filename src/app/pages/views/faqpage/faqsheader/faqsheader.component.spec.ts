import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaqsheaderComponent } from './faqsheader.component';

describe('FaqsheaderComponent', () => {
  let component: FaqsheaderComponent;
  let fixture: ComponentFixture<FaqsheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaqsheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaqsheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
