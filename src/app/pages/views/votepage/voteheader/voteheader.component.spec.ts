import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoteheaderComponent } from './voteheader.component';

describe('VoteheaderComponent', () => {
  let component: VoteheaderComponent;
  let fixture: ComponentFixture<VoteheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoteheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
