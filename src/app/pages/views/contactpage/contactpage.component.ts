import { Component, OnInit } from '@angular/core';
import { SeoService } from "src/app/services/seo/seo.service";

@Component({
	selector: "app-contactpage",
	templateUrl: "./contactpage.component.html",
	styleUrls: ["./contactpage.component.scss"],
})
export class ContactpageComponent implements OnInit {
	seoData: any = {
		title: "Contact | Ballantines - Vote For More",
		description: "",
		keywords: "",
	};

	constructor(private seo: SeoService) {
		this.seo.setSeo(this.seoData);
	}

	ngOnInit(): void {}
}
