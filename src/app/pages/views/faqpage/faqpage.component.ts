import { Component, OnInit } from '@angular/core';
import { SeoService } from "src/app/services/seo/seo.service";

@Component({
	selector: "app-faqpage",
	templateUrl: "./faqpage.component.html",
	styleUrls: ["./faqpage.component.scss"],
})
export class FaqpageComponent implements OnInit {
	seoData: any = {
		title: "FAQs | Ballantines - Vote For More",
		description: "",
		keywords: "",
	};

	constructor(private seo: SeoService) {
		this.seo.setSeo(this.seoData);
	}

	ngOnInit(): void {}
}
