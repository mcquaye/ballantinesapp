import { Component, OnInit } from '@angular/core';
import { SeoService } from "src/app/services/seo/seo.service";

@Component({
	selector: "app-pagenotfound",
	templateUrl: "./pagenotfound.component.html",
	styleUrls: ["./pagenotfound.component.scss"],
})
export class PagenotfoundComponent implements OnInit {
	seoData: any = {
		title: "404 Error Page Not Found | Ballantines - Vote For More",
		description: "",
		keywords: "",
	};

	constructor(private seo: SeoService) {
		this.seo.setSeo(this.seoData);
	}

	ngOnInit(): void {}
}
