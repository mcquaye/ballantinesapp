import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './pages/shared/nav/nav.component';
import { FooterComponent } from './pages/shared/footer/footer.component';
import { HomepageComponent } from './pages/views/homepage/homepage.component';
import { ContactpageComponent } from './pages/views/contactpage/contactpage.component';
import { FaqpageComponent } from './pages/views/faqpage/faqpage.component';
import { AboutpageComponent } from './pages/views/aboutpage/aboutpage.component';
import { VotepageComponent } from './pages/views/votepage/votepage.component';
import { PagenotfoundComponent } from './pages/views/pagenotfound/pagenotfound.component';
import { HomeheaderComponent } from './pages/views/homepage/homeheader/homeheader.component';
import { AboutheaderComponent } from './pages/views/aboutpage/aboutheader/aboutheader.component';
import { ContactheaderComponent } from './pages/views/contactpage/contactheader/contactheader.component';
import { VoteheaderComponent } from './pages/views/votepage/voteheader/voteheader.component';
import { FaqsheaderComponent } from './pages/views/faqpage/faqsheader/faqsheader.component';
import { NavMobileComponent } from './pages/shared/nav-mobile/nav-mobile.component';
import { SponsorsComponent } from './pages/views/sponsors/sponsors.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    HomepageComponent,
    ContactpageComponent,
    FaqpageComponent,
    AboutpageComponent,
    VotepageComponent,
    PagenotfoundComponent,
    HomeheaderComponent,
    AboutheaderComponent,
    ContactheaderComponent,
    VoteheaderComponent,
    FaqsheaderComponent,
    NavMobileComponent,
    SponsorsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
