import { Component, OnInit } from '@angular/core';
import { SeoService } from "src/app/services/seo/seo.service";

@Component({
	selector: "app-aboutpage",
	templateUrl: "./aboutpage.component.html",
	styleUrls: ["./aboutpage.component.scss"],
})
export class AboutpageComponent implements OnInit {
	seoData: any = {
		title: "Mechanics | Ballantines - Vote For More",
		description: "",
		keywords: "",
	};

	constructor(private seo: SeoService) {
		this.seo.setSeo(this.seoData);
	}

	ngOnInit(): void {}
}
